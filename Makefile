all: tbj toAtlas


OBJECTS =  actual.main.o Protos22/Red/tbj/main.f Protos22/Red/tbj/init.o Protos22/Red/tbj/evtgen.o Protos22/Red/tbj/matr.o Protos22/Red/tbj/run.o Protos22/Red/tbj/plots.o 
FC=gfortran
FFLAGS = -O2 -fautomatic -frecursive -IProtos22/Red/tbj
CXXFLAGS = -O2 -std=c++11
tbj: protos helas $(OBJECTS)
	g++ -Wl,-rpath=/usr/local/lib $(OBJECTS) -LProtos22/lib64 -L/usr/local/lib -lLHAPDF  -lprotos -ldhelas -lgfortran -pthread -o protos-jfb 


protos:
	cd Protos22/share; make FC=gfortran FFLAGS="-O2 -fautomatic -frecursive"


helas:
	cd Protos22/helas; make FC=gfortran FFLAGS="-O2 -fautomatic -frecursive"

