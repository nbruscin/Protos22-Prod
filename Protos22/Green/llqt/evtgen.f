      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      INTEGER ILEP1,ILEP2,ILEP3,IQFLAV
      COMMON /FSTATE/ ILEP1,ILEP2,ILEP3,IQFLAV
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET

!     External functions used

      REAL*8 XRN

!     Outputs

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3),P5(0:3),P6(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4,P5,P6
      REAL*8 Pt1(0:3),Pt2(0:3),PW1(0:3),PW2(0:3)
      COMMON /MOMINT/ Pt1,Pt2,PW1,PW2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      INTEGER IFCN_T,IFCN_TB
      COMMON /tflagF/ IFCN_T,IFCN_TB

!     Local variables

      REAL*8 EPS,y1,y2,flux
      REAL*8 PCM_LAB(0:3)
      REAL*8 Qt1,Qt2,QW1,QW2,m1,m2
      REAL*8 WT1,WT2,WT3,WT4,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC
      INTEGER IBW1,IBW2

!     --------------------------------------------

      WTPS=0d0

!     Selection of decay channels

      IF (IPROC .LE. 9) THEN
        IFCN_T = 1
        IFCN_TB = 0
      ELSE
        IFCN_T = 0
        IFCN_TB = 1
      ENDIF

      IF (IFCN_T .EQ. 0) THEN
        IBW1=1
      ELSE
        IBW1=0
      ENDIF

      IF (IFCN_TB .EQ. 0) THEN
        IBW2=1
      ELSE
        IBW2=0
      ENDIF

!     Generation of the masses of the virtual t, tbar, W+, W-
      
      CALL BREIT(mt,Gt,NGt,Qt1,WT1)
      IF (IBW1 .EQ. 1) THEN
        CALL BREIT(MW,GW,NGW,QW1,WT3)
        IF (Qt1 .LT. QW1+mb) RETURN
        WT1=WT1*WT3*(2d0*pi)**3
      ENDIF
      CALL BREIT(mt,Gt,NGt,Qt2,WT2)
      IF (IBW2 .EQ. 1) THEN
        CALL BREIT(MW,GW,NGW,QW2,WT4)
        IF (Qt2 .LT. QW2+mb) RETURN
        WT2=WT2*WT4*(2d0*pi)**3
      ENDIF
      WT_BREIT=WT1*WT2*(2d0*pi)**6

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(Qt1+Qt2)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    

      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system

      IDIR=0
      IF  (MOD(IPROC,10)+IPROC/10 .GT. 5) IDIR=1

      m1=0d0
      m2=0d0

      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Generation

      CALL PHASE2sym(SQRT(s),Qt1,Qt2,PCM_LAB,Pt1,Pt2,WT)      ! Main process
      WT_PROD=(2d0*pi)**4*WT

      IF (IFCN_T .EQ. 1) THEN
        CALL PHASE3(Qt1,0d0,0d0,0d0,Pt1,P1,P2,P3,WT)
        WT1=WT
      ELSE
        CALL PHASE2(Qt1,QW1,mb,Pt1,PW1,P3,WT)               ! t decay
        WT1=WT
        CALL PHASE2(QW1,0d0,0d0,PW1,P1,P2,WT)                ! W+ decay
        WT1=WT1*WT
      ENDIF

      IF (IFCN_TB .EQ. 1) THEN
        CALL PHASE3(Qt2,0d0,0d0,0d0,Pt2,P4,P5,P6,WT)
        WT2=WT
      ELSE
        CALL PHASE2(Qt2,QW2,mb,Pt2,PW2,P6,WT)               ! tbar decay
        WT2=WT
        CALL PHASE2(QW2,0d0,0d0,PW2,P4,P5,WT)                ! W- decay
        WT2=WT2*WT
      ENDIF
      WT_DEC=WT1*WT2

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3),P5(0:3),P6(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4,P5,P6
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local variables

!     --------------------------------------------

      INOT=1


98    INOT=0

      RETURN
      END





      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 GG_TT,UU_TT

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3),P5(0:3),P6(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4,P5,P6
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER i,j,k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=mt*QFAC

      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .EQ. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .EQ. 0) RETURN
      
      STR(1)=fg1*fg2          ! g  g  -  No factor here nor in global fact() 
      IF (IPPBAR .EQ. 0) THEN
      STR(2)=fu1*fus2
      STR(3)=fd1*fds2
      STR(4)=fs1*fs2
      STR(5)=fc1*fc2
      STR(6)=fus1*fu2
      STR(7)=fds1*fd2
      STR(8)=fs1*fs2
      STR(9)=fc1*fc2
      ELSE
      STR(2)=fu1*fu2
      STR(3)=fd1*fd2
      STR(4)=fs1*fs2
      STR(5)=fc1*fc2
      STR(6)=fus1*fus2
      STR(7)=fds1*fds2
      STR(8)=fs1*fs2
      STR(9)=fc1*fc2
      ENDIF

!     Rest of processes: the same PDFs

      DO i=1,9
      STR(i+9)=STR(i)
      ENDDO

!     Set "coloured" squared amplitudes to zero

      DO i=1,3
        DO j=1,MAXFL
          CAMP2(i,j)=0d0
        ENDDO
      ENDDO

!     Select helicity amplitude and process

      IAMP=IPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 100) ntry(IAMP) = 100

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (MOD(IPROC,10)+IPROC/10 .NE. 1) GOTO 12

!     g g -> t t~

      ME=0
      ICSTR=1
      CAMP2(ICSTR,0)=2
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GG_TT(Q1,Q2,P1,P2,P3,P4,P5,P6,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      
      GOTO 20

12    CONTINUE

!     q q~

      ME=0d0
      ICSTR=2
      IF (IDIR .EQ. 1) ICSTR=3
      CAMP2(ICSTR,0)=1
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 50) THEN
          M1=UU_TT(Q1,Q2,P1,P2,P3,P4,P5,P6,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END
