      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pq1(0:3),Pq2(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pq1,Pq2
      REAL*8 Pt(0:3),PW(0:3),PWX(0:3)
      COMMON /MOMINT/ Pt,PW,PWX
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2

!     Local variables

      REAL*8 EPS,y1,y2,flux
      REAL*8 PCM_LAB(0:3)
      REAL*8 Qt,QW,QWX,F1MASS,F2MASS,m1,m2
      REAL*8 WT1,WT2,WT3,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC
      INTEGER IL,ILQ

!     --------------------------------------------

      WTPS=0d0

!     Generation of the masses of the virtual t, W
      
      CALL BREIT(mt,Gt,NGt,Qt,WT1)
      CALL BREIT(MW,GW,NGW,QW,WT2)
      CALL BREIT(MW,GW,NGW,QWX,WT3)
      IF (Qt .LT. QW+mb) RETURN
      WT_BREIT=WT1*WT2*WT3*(2d0*pi)**9

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(Qt+QWX)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system

      IDIR=0
      IF (IPROC .GT. 2) IDIR=1

      m1=0d0
      m2=0d0
      
      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)


!     Select decay channel: Charged lepton (or quark, in full hadronic)

      IF (ILEP .LE. 3) THEN                              ! Semileptonic
        IL=ILEP
      ELSE IF ((ILEP .EQ. 4) .OR. (ILEP .EQ. 6)) THEN    ! Semilep sum or dilep
        y1=RAN2(idum)
        IF (y1 .LT. 1d0/3d0) THEN
          IL=1
        ELSE IF (y1 .LT. 2d0/3d0) THEN
          IL=2
        ELSE
          IL=3
        ENDIF
      ELSE                                               ! Full hadronic
        y1=RAN2(idum)
        IF (y1 .LT. 0.5d0) THEN
          IL=4
        ELSE
          IL=5
        ENDIF
      ENDIF

!     Select other fermion

      y1=RAN2(idum)
      IF (ILEP .LE. 5) THEN                              ! Semilep or full hadr
        IF (y1 .LT. 0.5d0) THEN
          ILQ=4
        ELSE
          ILQ=5
        ENDIF
      ELSE                                               ! Dileptonic
        IF (y1 .LT. 1d0/3d0) THEN
          ILQ=1
        ELSE IF (y1 .LT. 2d0/3d0) THEN
          ILQ=2
        ELSE
          ILQ=3
        ENDIF
      ENDIF

!     Select which W they come from

      IF1=IL
      IF2=ILQ
      y1=RAN2(idum)
      IF (y1 .LT. 0.5d0) THEN
        IF1=ILQ
        IF2=IL
      ENDIF

      F1MASS=0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0
      IF (IF2 .EQ. 3) F2MASS=mtau
      
!     Generation

      CALL PHASE2sym(SQRT(s),Qt,QWX,PCM_LAB,Pt,PWX,WT1)
      WT_PROD=(2d0*pi)**4*WT1

      CALL PHASE2(Qt,QW,mb,Pt,PW,Pb,WT)                     ! t decay
      WT_DEC=WT

      CALL PHASE2(QW,0d0,F1MASS,PW,Pn,Pl,WT)                 ! W decay
      WT_DEC=WT_DEC*WT

      CALL PHASE2(QWX,0d0,F2MASS,PWX,Pq1,Pq2,WT)               ! WX decay
      WT_DEC=WT_DEC*WT

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF/flux
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pq1(0:3),Pq2(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pq1,Pq2
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     --------------------------------------------

      INOT=1


98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 GB_TWM,GBB_TBWP

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pq1(0:3),Pq2(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pq1,Pq2
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC
      INTEGER IMATCH
      COMMON /MATCH/ IMATCH
      INTEGER IGEN
      COMMON /TMIX/ IGEN

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      REAL*8 fq1,fq2,fqb1,fqb2
      REAL*8 fbsub
      INTEGER k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=(mt+MW)*QFAC
     
      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      IF (IMATCH .EQ. 1) THEN
        CALL GET_BPDF(x1,fbsub)
        fb1=fb1-fbsub
      ENDIF
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      IF (IMATCH .EQ. 1) THEN
        CALL GET_BPDF(x2,fbsub)
        fb2=fb2-fbsub
      ENDIF

      IF (IGEN .EQ. 1) THEN
        fq1=fd1
        fqb1=fds1
        fq2=fd2
        fqb2=fds2
      ELSE IF (IGEN .EQ. 2) THEN
        fq1=fs1
        fqb1=fs1
        fq2=fs2
        fqb2=fs2
      ELSE
        fq1=fb1
        fqb1=fb1
        fq2=fb2
        fqb2=fb2
      ENDIF


      IF (IPPBAR .EQ. 0) THEN
      STR(1)=fg1*fq2           ! g  q 
      STR(2)=fg1*fqb2          ! g  q~
      STR(3)=fq1*fg2           ! q  g
      STR(4)=fqb1*fg2          ! q~ g
      ELSE
      STR(1)=fg1*fqb2          ! g  q 
      STR(2)=fg1*fq2           ! g  q~
      STR(3)=fq1*fg2           ! q  g
      STR(4)=fqb1*fg2          ! q~ g
      ENDIF

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IF1 .EQ. 3) IAMP=IAMP+NPROC
      IF (IF2 .EQ. 3) IAMP=IAMP+2*NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 100) ntry(IAMP) = 100

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF ((IPROC .NE. 1) .AND. (IPROC .NE. 3)) GOTO 12

!     g  b    b  g 

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GB_TWM(Q1,Q2,Pn,Pl,Pb,Pq1,Pq2,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

12    CONTINUE

!     g  b~    b~  g 

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GBB_TBWP(Q1,Q2,Pn,Pl,Pb,Pq1,Pq2,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      GOTO 20

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END

