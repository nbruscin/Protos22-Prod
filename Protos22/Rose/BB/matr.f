      REAL*8 FUNCTION GG_BB(NHEL)

!     PROCESS : g g  -> B Bbar with all decays

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=12)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     Other momenta

      REAL*8 P1(0:3),P2(0:3)               ! Different name!!!
      COMMON /MOMINI/ P1,P2
      REAL*8 PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ PB1,PB2
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3),
     &  Pf3(0:3),Pfb3(0:3),Pf4(0:3),Pfb4(0:3)
      COMMON /MOMEXT/ Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,Pf3,Pfb3,Pf4,Pfb4

!     LOCAL VARIABLES 

      INTEGER I,J,IPOS
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)
      COMPLEX*16 Wb(6),Wbb(6),Wt(6),Wtb(6),WB1(6),WB2(6),WW1(6),WW2(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6)
      COMPLEX*16 Wf3(6),Wfb3(6),Wf4(6),Wfb4(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IB1,IB2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IZF1,IZF2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWtQ(2),GZQb(2),Xmix
      COMPLEX*16 GHQb(2),GHbQ(2)

!     Specific couplings

      REAL*8 GB1ff(2),GB2ff(2)

!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (IB1 .NE. 2) GOTO 2

      IF (IZF1 .EQ. 0) THEN
        GB1ff(1)=GZvv(1)
        GB1ff(2)=GZvv(2)
      ELSE IF (IZF1 .EQ. 1) THEN
        GB1ff(1)=GZll(1)
        GB1ff(2)=GZll(2)
      ELSE IF (IZF1 .EQ. 2) THEN
        GB1ff(1)=GZuu(1)
        GB1ff(2)=GZuu(2)
      ELSE IF (IZF1 .EQ. 3) THEN
        GB1ff(1)=GZdd(1)
        GB1ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF1 = ',IZF1
        STOP
      ENDIF

2     IF (IB2 .NE. 2) GOTO 4

      IF (IZF2 .EQ. 0) THEN
        GB2ff(1)=GZvv(1)
        GB2ff(2)=GZvv(2)
      ELSE IF (IZF2 .EQ. 1) THEN
        GB2ff(1)=GZll(1)
        GB2ff(2)=GZll(2)
      ELSE IF (IZF2 .EQ. 2) THEN
        GB2ff(1)=GZuu(1)
        GB2ff(2)=GZuu(2)
      ELSE IF (IZF2 .EQ. 3) THEN
        GB2ff(1)=GZdd(1)
        GB2ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF2 = ',IZF2
        STOP
      ENDIF

4     CONTINUE

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                            
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)                            
      CALL OXXXXX(Pb,mb,NHEL(3),1,Wb)                     ! b
      CALL IXXXXX(Pbb,mb,NHEL(4),-1,Wbb)                  ! bbar
      IPOS=5

!     First heavy quark

      IF (IB1 .EQ. 1) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(IPOS),1,Wf1)             ! f1     - W- from B decay
        CALL IXXXXX(Pfb1,0d0,NHEL(IPOS+1),-1,Wfb1)        ! f1bar
        CALL OXXXXX(Pf3,0d0,NHEL(IPOS+2),1,Wf3)           ! f3     - W+ from t decay
        CALL IXXXXX(Pfb3,0d0,NHEL(IPOS+3),-1,Wfb3)        ! f3bar
        IPOS=IPOS+4
        CALL JIOXXX(Wfb3,Wf3,GWF,MW,GW,WW1)               ! W+
        CALL FVOXXX(Wb,WW1,GWF,mt,Gt,Wt)                  ! t
        CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)               ! W-
        CALL FVOXXX(Wt,WB1,GWtQ,mQ,GQ,W3)                 ! B
      ELSE IF (IB1 .EQ. 2) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(IPOS),1,Wf1)             ! f1     - Z from B decay
        CALL IXXXXX(Pfb1,0d0,NHEL(IPOS+1),-1,Wfb1)        ! f1bar
        IPOS=IPOS+2
        CALL JIOXXX(Wfb1,Wf1,GB1ff,MZ,GZ,WB1)             ! Z
        CALL FVOXXX(Wb,WB1,GZQb,mQ,GQ,W3)                 ! B
      ELSE IF (IB1 .EQ. 3) THEN
        CALL SXXXXX(PB1,1,WB1)                            ! H
        CALL FSOXXX(Wb,WB1,GHQb,mQ,GQ,W3)                 ! B
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in GG_BB'
        STOP
      ENDIF

!     Second heavy quark

      IF (IB2 .EQ. 1) THEN
        CALL OXXXXX(Pf2,0d0,NHEL(IPOS),1,Wf2)             ! f2     - W+ from Bbar decay
        CALL IXXXXX(Pfb2,0d0,NHEL(IPOS+1),-1,Wfb2)        ! f2bar
        CALL OXXXXX(Pf4,0d0,NHEL(IPOS+2),1,Wf4)           ! f4     - W- from tbar decay
        CALL IXXXXX(Pfb4,0d0,NHEL(IPOS+3),-1,Wfb4)        ! f4bar

        CALL JIOXXX(Wfb4,Wf4,GWF,MW,GW,WW2)               ! W-
        CALL FVIXXX(Wbb,WW2,GWF,mt,Gt,Wtb)                ! tbar
        CALL JIOXXX(Wfb2,Wf2,GWF,MW,GW,WB2)               ! W+
        CALL FVIXXX(Wtb,WB2,GWtQ,mQ,GQ,W4)                ! Bbar
      ELSE IF (IB2 .EQ. 2) THEN
        CALL OXXXXX(Pf2,0d0,NHEL(IPOS),1,Wf2)             ! f1     - Z from Bbar decay
        CALL IXXXXX(Pfb2,0d0,NHEL(IPOS+1),-1,Wfb2)        ! f1bar

        CALL JIOXXX(Wfb2,Wf2,GB2ff,MZ,GZ,WB2)             ! Z
        CALL FVIXXX(Wbb,WB2,GZQb,mQ,GQ,W4)                ! Bbar
      ELSE IF (IB2 .EQ. 3) THEN
        CALL SXXXXX(PB2,1,WB2)                            ! H
        CALL FSIXXX(Wbb,WB2,GHbQ,mQ,GQ,W4)                ! Bbar
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in GG_BB'
        STOP
      ENDIF

!     Amplitudes

      CALL FVOXXX(W3,W2,GG,mQ,GQ,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_BB = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_BB = GG_BB + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_BB = GG_BB/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END



      REAL*8 FUNCTION UU_BB(NHEL)

!     FOR PROCESS : q qbar  -> B Bbar with all decays

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=12)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     Other momenta

      REAL*8 P1(0:3),P2(0:3)               ! Different name!!!
      COMMON /MOMINI/ P1,P2
      REAL*8 PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ PB1,PB2
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3),
     &  Pf3(0:3),Pfb3(0:3),Pf4(0:3),Pfb4(0:3)
      COMMON /MOMEXT/ Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,Pf3,Pfb3,Pf4,Pfb4

!     LOCAL VARIABLES 

      INTEGER I,J,IPOS
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 Wb(6),Wbb(6),Wt(6),Wtb(6),WB1(6),WB2(6),WW1(6),WW2(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6)
      COMPLEX*16 Wf3(6),Wfb3(6),Wf4(6),Wfb4(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IB1,IB2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IZF1,IZF2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWtQ(2),GZQb(2),Xmix
      COMPLEX*16 GHQb(2),GHbQ(2)

!     Specific couplings

      REAL*8 GB1ff(2),GB2ff(2)

!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (IB1 .NE. 2) GOTO 2

      IF (IZF1 .EQ. 0) THEN
        GB1ff(1)=GZvv(1)
        GB1ff(2)=GZvv(2)
      ELSE IF (IZF1 .EQ. 1) THEN
        GB1ff(1)=GZll(1)
        GB1ff(2)=GZll(2)
      ELSE IF (IZF1 .EQ. 2) THEN
        GB1ff(1)=GZuu(1)
        GB1ff(2)=GZuu(2)
      ELSE IF (IZF1 .EQ. 3) THEN
        GB1ff(1)=GZdd(1)
        GB1ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF1 = ',IZF1
        STOP
      ENDIF

2     IF (IB2 .NE. 2) GOTO 4

      IF (IZF2 .EQ. 0) THEN
        GB2ff(1)=GZvv(1)
        GB2ff(2)=GZvv(2)
      ELSE IF (IZF2 .EQ. 1) THEN
        GB2ff(1)=GZll(1)
        GB2ff(2)=GZll(2)
      ELSE IF (IZF2 .EQ. 2) THEN
        GB2ff(1)=GZuu(1)
        GB2ff(2)=GZuu(2)
      ELSE IF (IZF2 .EQ. 3) THEN
        GB2ff(1)=GZdd(1)
        GB2ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF2 = ',IZF2
        STOP
      ENDIF

4     CONTINUE

!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)                       
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                       
      CALL OXXXXX(Pb,mb,NHEL(3),1,Wb)                     ! b
      CALL IXXXXX(Pbb,mb,NHEL(4),-1,Wbb)                  ! bbar
      IPOS=5

!     First heavy quark

      IF (IB1 .EQ. 1) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(IPOS),1,Wf1)             ! f1     - W- from B decay
        CALL IXXXXX(Pfb1,0d0,NHEL(IPOS+1),-1,Wfb1)        ! f1bar
        CALL OXXXXX(Pf3,0d0,NHEL(IPOS+2),1,Wf3)           ! f3     - W+ from t decay
        CALL IXXXXX(Pfb3,0d0,NHEL(IPOS+3),-1,Wfb3)        ! f3bar
        IPOS=IPOS+4
        CALL JIOXXX(Wfb3,Wf3,GWF,MW,GW,WW1)               ! W+
        CALL FVOXXX(Wb,WW1,GWF,mt,Gt,Wt)                  ! t
        CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)               ! W-
        CALL FVOXXX(Wt,WB1,GWtQ,mQ,GQ,W3)                 ! B
      ELSE IF (IB1 .EQ. 2) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(IPOS),1,Wf1)             ! f1     - Z from B decay
        CALL IXXXXX(Pfb1,0d0,NHEL(IPOS+1),-1,Wfb1)        ! f1bar
        IPOS=IPOS+2
        CALL JIOXXX(Wfb1,Wf1,GB1ff,MZ,GZ,WB1)             ! Z
        CALL FVOXXX(Wb,WB1,GZQb,mQ,GQ,W3)                 ! B
      ELSE IF (IB1 .EQ. 3) THEN
        CALL SXXXXX(PB1,1,WB1)                            ! H
        CALL FSOXXX(Wb,WB1,GHQb,mQ,GQ,W3)                 ! B
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in GG_BB'
        STOP
      ENDIF

!     Second heavy quark

      IF (IB2 .EQ. 1) THEN
        CALL OXXXXX(Pf2,0d0,NHEL(IPOS),1,Wf2)             ! f2     - W+ from Bbar decay
        CALL IXXXXX(Pfb2,0d0,NHEL(IPOS+1),-1,Wfb2)        ! f2bar
        CALL OXXXXX(Pf4,0d0,NHEL(IPOS+2),1,Wf4)           ! f4     - W- from tbar decay
        CALL IXXXXX(Pfb4,0d0,NHEL(IPOS+3),-1,Wfb4)        ! f4bar

        CALL JIOXXX(Wfb4,Wf4,GWF,MW,GW,WW2)               ! W-
        CALL FVIXXX(Wbb,WW2,GWF,mt,Gt,Wtb)                ! tbar
        CALL JIOXXX(Wfb2,Wf2,GWF,MW,GW,WB2)               ! W+
        CALL FVIXXX(Wtb,WB2,GWtQ,mQ,GQ,W4)                ! Bbar
      ELSE IF (IB2 .EQ. 2) THEN
        CALL OXXXXX(Pf2,0d0,NHEL(IPOS),1,Wf2)             ! f1     - Z from Bbar decay
        CALL IXXXXX(Pfb2,0d0,NHEL(IPOS+1),-1,Wfb2)        ! f1bar

        CALL JIOXXX(Wfb2,Wf2,GB2ff,MZ,GZ,WB2)             ! Z
        CALL FVIXXX(Wbb,WB2,GZQb,mQ,GQ,W4)                ! Bbar
      ELSE IF (IB2 .EQ. 3) THEN
        CALL SXXXXX(PB2,1,WB2)                            ! H
        CALL FSIXXX(Wbb,WB2,GHbQ,mQ,GQ,W4)                ! Bbar
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in GG_BB'
        STOP
      ENDIF

!     Amplitudes

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))

      CAMP(1) = -AMP(1)

      UU_BB = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_BB = UU_BB + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_BB = UU_BB/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END

