!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Z couplings to SM fermions

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

!     Gluon coupling

      GG(1)=-gs
      GG(2)=-gs

!     Heavy quark couplings
      
      Xmix=Vmix

      IF (IMOD .EQ. 1) THEN
        GWQb(1)=GWF(1)*Vmix
        GWQb(2)=0d0
        GZQt(1)=-gcw/2d0*Xmix
        GZQt(2)=0d0
        GHQt(1)=-g*mt/(2d0*MW)*Xmix                   ! T in t out
        GHQt(2)=-g*mQ/(2d0*MW)*Xmix
      ELSE IF (IMOD .EQ. 2) THEN                      ! (T B) doublet equal mixing
        GWQb(1)=0d0
        GWQb(2)=GWF(1)*Vmix
        GZQt(1)=0d0
        GZQt(2)=-gcw/2d0*Xmix
        GHQt(1)=g*mQ/(2d0*MW)*Xmix                    ! T in t out
        GHQt(2)=g*mt/(2d0*MW)*Xmix
      ELSE IF (IMOD .EQ. 3) THEN                      ! (T B) doublet up mixing
        GWQb(1)=0d0
        GWQb(2)=0d0
        GZQt(1)=0d0
        GZQt(2)=-gcw/2d0*Xmix
        GHQt(1)=g*mQ/(2d0*MW)*Xmix                    ! T in t out
        GHQt(2)=g*mt/(2d0*MW)*Xmix
      ELSE IF (IMOD .EQ. 4) THEN                      ! (X T) doublet
        GWQb(1)=0d0
        GWQb(2)=0d0
        GZQt(1)=0d0
        GZQt(2)=gcw/2d0*Xmix
        GHQt(1)=g*mQ/(2d0*MW)*Xmix                    ! T in t out
        GHQt(2)=g*mt/(2d0*MW)*Xmix
      ENDIF

      GHtQ(1)=CONJG(GHQt(2))                          ! t in T out
      GHtQ(2)=CONJG(GHQt(1))


