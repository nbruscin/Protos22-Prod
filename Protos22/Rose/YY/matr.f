      REAL*8 FUNCTION GG_YY(P1,P2,Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,NHEL)

!     PROCESS : g g  -> Y Ybar with decay to WbWb

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)
      COMPLEX*16 Wb(6),Wbb(6),WB1(6),WB2(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),Xmix

!     Specific couplings

!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------


!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                            
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)                            
      CALL OXXXXX(Pb,mb,NHEL(3),1,Wb)                     ! b
      CALL IXXXXX(Pbb,mb,NHEL(4),-1,Wbb)                  ! bbar

!     First heavy quark

      CALL OXXXXX(Pf1,0d0,NHEL(5),1,Wf1)                ! f1     - W- from Q decay
      CALL IXXXXX(Pfb1,0d0,NHEL(6),-1,Wfb1)             ! f1bar
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)               ! W+ prim
      CALL FVOXXX(Wb,WB1,GWQb,mQ,GQ,W3)                 ! Q

!     Second heavy quark

      CALL OXXXXX(Pf2,0d0,NHEL(7),1,Wf2)                ! f2     - W+ from Qbar decay
      CALL IXXXXX(Pfb2,0d0,NHEL(8),-1,Wfb2)            ! f2bar
      CALL JIOXXX(Wfb2,Wf2,GWF,MW,GW,WB2)               ! W+ prim
      CALL FVIXXX(Wbb,WB2,GWQb,mQ,GQ,W4)                ! Qbar

!     Amplitudes

      CALL FVOXXX(W3,W2,GG,mQ,GQ,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_YY = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_YY = GG_YY + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_YY = GG_YY/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END



      REAL*8 FUNCTION UU_YY(P1,P2,Pb,Pbb,Pf1,Pfb1,Pf2,Pfb2,NHEL)

!     FOR PROCESS : q qbar  -> Y Ybar with decay to WbWb

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 Pb(0:3),Pbb(0:3),Pf1(0:3),Pfb1(0:3),Pf2(0:3),Pfb2(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 Wb(6),Wbb(6),WB1(6),WB2(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),Xmix

!     Specific couplings


!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------


!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)                       
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                       
      CALL OXXXXX(Pb,mb,NHEL(3),1,Wb)                     ! b
      CALL IXXXXX(Pbb,mb,NHEL(4),-1,Wbb)                  ! bbar

!     First heavy quark

      CALL OXXXXX(Pf1,0d0,NHEL(5),1,Wf1)                ! f1     - W- from Q decay
      CALL IXXXXX(Pfb1,0d0,NHEL(6),-1,Wfb1)             ! f1bar
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)               ! W+ prim
      CALL FVOXXX(Wb,WB1,GWQb,mQ,GQ,W3)                 ! Q

!     Second heavy quark

      CALL OXXXXX(Pf2,0d0,NHEL(7),1,Wf2)                ! f2     - W+ from Qbar decay
      CALL IXXXXX(Pfb2,0d0,NHEL(8),-1,Wfb2)            ! f2bar
      CALL JIOXXX(Wfb2,Wf2,GWF,MW,GW,WB2)               ! W+ prim
      CALL FVIXXX(Wbb,WB2,GWQb,mQ,GQ,W4)                ! Qbar

!     Amplitudes

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))

      CAMP(1) = -AMP(1)

      UU_YY = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_YY = UU_YY + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_YY = UU_YY/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END

