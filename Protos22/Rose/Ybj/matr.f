      REAL*8 FUNCTION Ybj(NHEL)

!     FOR PROCESS : g d  -> Y b~ u  / g u~ -> Y b~ d~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=7)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),WB1(6),Wb(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IQ
      COMMON /Qflags/ IQ

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),Xmix

!     Specific couplings


!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL IXXXXX(PbX,mb,NHEL(7),-1,W5)             ! bbar

      IF (IQ .EQ. 1) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! d
        CALL OXXXXX(Pj,0d0,NHEL(6),1,W4)            ! u
      ELSE IF (IQ .EQ. -2) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! ubar
        CALL IXXXXX(Pj,0d0,NHEL(6),-1,W4)           ! dbar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Yj'
        STOP
      ENDIF

      CALL OXXXXX(Pf1,0d0,NHEL(3),1,Wf1)            ! e-
      CALL IXXXXX(Pfb1,0d0,NHEL(4),-1,Wfb1)         ! nu~
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)           ! W-
      CALL OXXXXX(Pb,mb,NHEL(5),1,Wb)               ! b
      CALL FVOXXX(Wb,WB1,GWQb,mQ,GQ,W3)             ! Y

      IF (IQ .EQ. 1) THEN
        CALL JIOXXX(W2,W4,GWF,MW,GW,W7)
      ELSE
        CALL JIOXXX(W4,W2,GWF,MW,GW,W7)
      ENDIF

      CALL FVIXXX(W5,W1,GG,mb,0d0,W6)
      CALL IOVXXX(W6,W3,W7,GWQb,AMP(1))
 
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W5,W8,W7,GWQb,AMP(2))

      Ybj = 0d0
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Ybj = Ybj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



      REAL*8 FUNCTION Ybarbj(NHEL)

!     FOR PROCESS : g u -> Y~ b d  /  g d~ -> Y~ b u~

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=7)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     External momenta

      REAL*8 P1(0:3),P2(0:3)         !!! Different name !!!
      COMMON /MOMINI/ P1,P2
      REAL*8 Pf1(0:3),Pfb1(0:3),Pb(0:3),Pj(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pf1,Pfb1,Pb,Pj,PbX

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6),W7(6),W8(6)
      COMPLEX*16 Wf1(6),Wfb1(6),WB1(6),Wb(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IQ
      COMMON /Qflags/ IQ

!     General couplings

      REAL*8 GWF(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),Xmix

!     Specific couplings


!     Colour data

      DATA EIGEN_VAL(1) /0.5d0 /
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

!     ------------------------
!     Define general couplings
!     ------------------------

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)             ! g
      CALL OXXXXX(PbX,mb,NHEL(7),1,W5)              ! b

      IF (IQ .EQ. 2) THEN
        CALL IXXXXX(P2,0d0,NHEL(2),1,W2)            ! u
        CALL OXXXXX(Pj,0d0,NHEL(6),1,W4)            ! d
      ELSE IF (IQ .EQ. -1) THEN
        CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)           ! dbar
        CALL IXXXXX(Pj,0d0,NHEL(6),-1,W4)           ! ubar
      ELSE
        PRINT *,'Wrong IQ = ',IQ,' in Ybarj'
        STOP
      ENDIF

      CALL OXXXXX(Pf1,0d0,NHEL(3),1,Wf1)            ! nu
      CALL IXXXXX(Pfb1,0d0,NHEL(4),-1,Wfb1)         ! e+
      CALL JIOXXX(Wfb1,Wf1,GWF,MW,GW,WB1)           ! W+
      CALL IXXXXX(Pb,mb,NHEL(5),-1,Wb)              ! b~
      CALL FVIXXX(Wb,WB1,GWQb,mQ,GQ,W3)             ! Y~

      IF (IQ .EQ. 2) THEN
        CALL JIOXXX(W2,W4,GWF,MW,GW,W7)
      ELSE
        CALL JIOXXX(W4,W2,GWF,MW,GW,W7)
      ENDIF

      CALL FVOXXX(W5,W1,GG,mb,0d0,W6)
      CALL IOVXXX(W3,W6,W7,GWQb,AMP(1))

      CALL FVIXXX(W3,W1,GG,mQ,GQ,W8)
      CALL IOVXXX(W8,W5,W7,GWQb,AMP(2))

      Ybarbj = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          Ybarbj = Ybarbj + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

