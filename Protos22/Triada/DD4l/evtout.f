      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4(0:3)
      COMMON /MOMEXT/ P1,P2,P3,P4
      INTEGER IP,IL1,IL2,IL3,IL4,IQ
      COMMON /Dflags/ IP,IL1,IL2,IL3,IL4,IQ
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MD,GD
      COMMON /Dmass/ MD,GD
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables

      INTEGER IDTAB1(8),IDTAB2(8),IDTABL(3),IDTABN(3)
      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2
      INTEGER i,IPX

!                    IP = 1,3      IP = 2
      DATA IDTAB1  / 2, 1, 4, 3,   2, 1, 4, 3/
      DATA IDTAB2  /-2,-1,-4,-3,  -1,-2,-3,-4/
      DATA IDTABL  /-11,-13,-15/
      DATA IDTABN  / 12, 14, 16/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0
      
      RETURN

10    IF (IMODE .NE. 0) GOTO 11
      
!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)/1000d0                          ! In pb
      IF (XWGTUP .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,XWGTUP)
      NUMEVT=NUMEVT+1

!     Flavour

      IPX=MOD(IPROC,5)+IPROC/5
      IF (IP .EQ. 2) IPX=IPX+4
      ID(1)=IDTAB1(IPX)
      ID(2)=IDTAB2(IPX)

      IF (IP .EQ. 1) THEN
        ID(3)=IDTABL(IL1)
        ID(4)=IDTABL(IL2)
        ID(5)=-IDTABL(IL3)
        ID(6)=-IDTABL(IL4)
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 1)) THEN
        ID(3)=IDTABL(IL1)
        ID(4)=IDTABL(IL2)
        ID(5)=-IDTABL(IL3)
        ID(6)=-IDTABN(IL4)
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 2)) THEN
        ID(3)=IDTABL(IL1)
        ID(4)=IDTABN(IL2)
        ID(5)=-IDTABL(IL3)
        ID(6)=-IDTABL(IL4)
      ELSE IF (IP .EQ. 3) THEN
        ID(3)=IDTABL(IL1)
        ID(4)=IDTABN(IL2)
        ID(5)=-IDTABL(IL3)
        ID(6)=-IDTABN(IL4)
      ENDIF

!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

      DO i=1,2
        IF (ID(i) .GT. 0) THEN         ! Initial q / qbar and b / bbar
           ICOL1(i)=1
        ELSE
           ICOL2(i)=1
        ENDIF
      ENDDO

      IF (IDIR .EQ. 0) THEN
        Pz1=Q1(3)
        Pz2=Q2(3)
      ELSE
        Pz1=Q2(3)
        Pz2=Q1(3)
      ENDIF
      
      WRITE (35,3010) NUMEVT,XWGTUP,Q
      IF (Pz1 .GT. 0d0) THEN
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      ELSE
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      ENDIF
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),P1(1),P1(2),P1(3)
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),P2(1),P2(2),P2(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),P3(1),P3(2),P3(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),P4(1),P4(2),P4(3)
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 40+IP
      WRITE (36,4001) ET
      WRITE (36,4005) MD,GD
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_tot/1000d0,ERR_tot/1000d0
      CLOSE (36)
      RETURN



12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)
   
4000  FORMAT (I2,'                       ',
     & '        ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F9.6,
     &  '                ! MD, GD')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! total cross section and error')
      END

