      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Input triplet parameters 

      REAL*8 MD,GD
      COMMON /Dmass/ MD,GD

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER IP,IL1,IL2,IL3,IL4,IQ
      COMMON /Dflags/ IP,IL1,IL2,IL3,IL4,IQ

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      COMPLEX*16 Y(3,3)
      REAL*8 Ysum
      COMMON /DLcoup/ Y,Ysum
      REAL*8 BRch(9),BRch_ac(0:9),WTch(9)
      COMMON /Ddecay/ BRch,BRch_ac,WTch
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGD
      COMMON /BWdata/ NGD
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(16*NPROC,3**NPART)
      INTEGER ncomb,ntry(16*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry

!     Neutrino data

      REAL*8 dm21,dm31,m0,s12,s23,s13,delta,beta2,beta3
      REAL*8 c12,c23,c13,m1,m2,m3,v_d
      COMPLEX*16 Eid,Eib2,Eib3

!     To be saved 

      COMPLEX*16 V(3,3)
      SAVE V

!     Local variables

      INTEGER i,j,k
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 xW,xb,EW,qW
      REAL*8 gevpb
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     triplet and neutrino parameters
!
      OPEN (31,FILE='input/triplet.dat',status='old')
      READ (31,*) v_d
      READ (31,*) dm21
      READ (31,*) dm31
      READ (31,*) m0
      READ (31,*) s12
      READ (31,*) s23
      READ (31,*) s13
      READ (31,*) delta
      READ (31,*) beta2
      READ (31,*) beta3
      CLOSE (31)
 
      c12=SQRT(1d0-s12**2)
      c23=SQRT(1d0-s23**2)
      c13=SQRT(1d0-s13**2)
      Eid=COS(delta)+(0d0,1d0)*SIN(delta)
      Eib2=COS(beta2)+(0d0,1d0)*SIN(beta2)
      Eib3=COS(beta3)+(0d0,1d0)*SIN(beta3)
      V(1,1) = c12*c13
      V(1,2) = s12*c13
      V(1,3) = s13*CONJG(Eid)
      V(2,1) = -s12*c23 - c12*s23*s13*Eid
      V(2,2) = c12*c23 - s12*s23*s13*Eid 
      V(2,3) = s23*c13
      V(3,1) = s12*s23 - c12*c23*s13*Eid
      V(3,2) = -c12*s23 - s12*c23*s13*Eid
      V(3,3) =  c23*c13
      IF (dm31 .GT. 0) THEN
        m1=m0
        m2=SQRT(m1**2+dm21)
        m3=SQRT(m1**2+dm31)
      ELSE
        m3=m0
        m1=SQRT(m3**2-dm31)
        m2=SQRT(m1**2+dm21)
      ENDIF
      DO i=1,3
        DO j=1,3
          Y(i,j)=CONJG(V(i,1))*m1*CONJG(V(j,1))
     &      + CONJG(V(i,2))*m2*Eib2*CONJG(V(j,2))
     &      + CONJG(V(i,3))*m3*Eib3*CONJG(V(j,3))
          Y(i,j)=Y(i,j)/(2d0*v_d)
        ENDDO
      ENDDO

!     Calculate branching ratios and their weight

      Ysum=0d0
      DO i=1,3
        DO j=1,3
          Ysum=Ysum+ABS(Y(i,j))**2
        ENDDO
      ENDDO

      BRch_ac(0)=0d0
      DO i=1,3
        DO j=1,3
        k=3*(i-1)+j
          BRch(k)=ABS(Y(i,j))**2/Ysum
          BRch_ac(k)=BRch_ac(k-1)+BRch(k)
          WTch(k)=1d0/BRch(k)
        ENDDO
      ENDDO
      IF (ABS(BRch_ac(9)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRch_ac(9)
      BRch_ac(9)=1d0

!
!     Fixed parameters
!

      gevpb=3.8938d8

!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb*1000d0                   ! Cross section in fb
        IF (IP .EQ. 1) fact(i)=fact(i)/4d0     ! Symm factor D++ D--
        IF (IP .EQ. 2) fact(i)=fact(i)/2d0     ! Symm factor D++ D-
      ENDDO
      
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(MD)
      CALL SETALPHAS(MD)
!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)

!     Z boson

      cw=SQRT(1d0-sw2)
      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)

!     Higgs  -- not used

      GH=1d0

!     Triplet

      GD=1d0/(8d0*pi)*MD*Ysum
      NGD=0.95*MD/GD

      PRINT 1100,MD,GD
      PRINT 999
      PRINT 1200,ABS(Y(1,1))**2/Ysum
      PRINT 1201,(ABS(Y(1,2))**2+ABS(Y(2,1))**2)/Ysum
      PRINT 1202,(ABS(Y(1,3))**2+ABS(Y(3,1))**2)/Ysum
      PRINT 1203,ABS(Y(2,2))**2/Ysum
      PRINT 1204,(ABS(Y(2,3))**2+ABS(Y(3,2))**2)/Ysum
      PRINT 1205,ABS(Y(3,3))**2/Ysum
      PRINT 999

!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO

      CALL SETPOL(istep,nhel,ncomb)
      DO i=1,16*NPROC
        DO j=1,ncomb
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

      RETURN

500   FORMAT (' _____    _           _       ')
501   FORMAT ('|_   _|  (_)         | |      ')
502   FORMAT ('  | |_ __ _  __ _  __| | __ _ ')
503   FORMAT ('  | | \'__| |/ _` |/ _` |/ _` |')
504   FORMAT ('  | | |  | | (_| | (_| | (_| |')
505   FORMAT ('  \\_/_|  |_|\\__,_|\\__,_|\\__,_|')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
999   FORMAT ('')
1000  FORMAT ('---------------------------------------')
1100  FORMAT ('MD = ',F6.1,'   GD = ',F9.6)
1200  FORMAT ('BR(D -> ee) = ',F6.4)
1201  FORMAT ('BR(D -> em) = ',F6.4)
1202  FORMAT ('BR(D -> et) = ',F6.4)
1203  FORMAT ('BR(D -> mm) = ',F6.4)
1204  FORMAT ('BR(D -> mt) = ',F6.4)
1205  FORMAT ('BR(D -> tt) = ',F6.4)
1303  FORMAT ('Warning: in initialisation acc sum = ',D9.4)
      END


      SUBROUTINE SETPOL(nspin,hel,nhel)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,NPART
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,NPART
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=NPART
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,NPART
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END




