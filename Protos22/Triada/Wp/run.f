      SUBROUTINE CHGPAR(id,step)
      IMPLICIT NONE

!     Arguments

      INTEGER id
      REAL*8 step

!     Parameters that can be changed

      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VL(3),VR(3)
      COMMON /Ncoup/ VL,VR
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP

      IF (id .EQ. -1) THEN
        idum0=idum0+1
      ELSE IF (id .EQ. 1) THEN
        mWP=mWP+step
      ELSE IF (id .EQ. 2) THEN
        mN=mN+step
      ELSE IF (id .EQ. 3) THEN
        VL(1)=VL(1)+step
      ELSE IF (id .EQ. 4) THEN
        VL(2)=VL(2)+step
      ELSE IF (id .EQ. 5) THEN
        VL(3)=VL(3)+step
      ELSE
        print *,id
        PRINT 100
        STOP
      ENDIF

      RETURN

100   FORMAT ('Unsupported parameter scan')
      END


      SUBROUTINE LOGINI(IMODE,id)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE,id

!     For logging

      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VL(3),VR(3)
      COMMON /Ncoup/ VL,VR
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l

!     Local

      REAL*8 scpar

      IF (IMODE .EQ. -1) THEN 
        OPEN (40,FILE='output/'//PROCNAME(1:l)//'.log',
     &    status='unknown')
        RETURN
      ELSE IF (IMODE .EQ. 1) THEN
        CLOSE (40)
        RETURN
      ELSE IF (IMODE .NE. 0) THEN
        PRINT 99
        STOP
      ENDIF

!     Write results

      IF ((id .EQ. 0) .OR. (id .EQ. -1)) THEN
        scpar=0d0
      ELSE IF (id .EQ. 1) THEN
        scpar=MWP
      ELSE IF (id .EQ. 2) THEN
        scpar=mN
      ELSE IF (id .EQ. 3) THEN
        scpar=VL(1)
      ELSE IF (id .EQ. 4) THEN
        scpar=VL(2)
      ELSE IF (id .EQ. 5) THEN
        scpar=VL(3)
      ELSE
        PRINT 100
        STOP
      ENDIF

      WRITE (40,200) scpar,SIG_tot

      RETURN
99    FORMAT ('Error in WRITELOG call')
100   FORMAT ('Unsupported parameter scan')
200   FORMAT (F9.4,' ',D12.6)
      END


