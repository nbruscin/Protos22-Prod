      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Input parameters 

      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VlN(3)
      COMMON /Ncoup/ VlN
      REAL*8 MZP,GZP
      COMMON /ZPmass/ MZP,GZP
      INTEGER IZPMOD
      COMMON /ZPmod/ IZPMOD

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      INTEGER IDW1,IDZ1,IDH1,IDW2,IDZ2,IDH2,IDLNC,IDLNV
      COMMON /Nfstate/ IDW1,IDZ1,IDH1,IDW2,IDZ2,IDH2,IDLNC,IDLNV

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN
      COMMON /ZPcoup/ gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      REAL*8 WTT1(3),WTT2(3),WTFL(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRT2ac(0:3),BRFLac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTT2,WTFL,WTZ,BRT1ac,BRT2ac,BRFLac,BRZac
      REAL*8 BRN
      COMMON /Zdec/ BRN

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGN,NGW,NGZ
      COMMON /BWdata/ NGN,NGW,NGZ
      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &        nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,nhel4,GOODHEL,
     &  ncomb8,ncomb6,ncomb4,ntry

!     Variables for weighting of final states

      REAL*8 PW_T1,PW_T2
      REAL*8 PRT1(3),PRT2(3),PRZ(4),PRFL(3)

!     Local variables

      INTEGER i,j
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 GN_Wl,GN_Zv,GN_Hv
      REAL*8 Ynorm
      REAL*8 GZp_uc,GZp_t,GZp_dsb,GZP_emt,GZP_v,GZP_N
      REAL*8 xW,xb,EW,qW,xt,xN
      REAL*8 r_W,r_Z,r_H,Vsq
      REAL*8 gevpb
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb*1000d0                   ! Cross section in fb
        IF (IMA .EQ. 1) fact(i)=fact(i)/2d0
      ENDDO
      
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(MZP)
      CALL SETALPHAS(MZP)
      cw=SQRT(1d0-sw2)

!     Select Z' model

      IF (IZPMOD .EQ. 0) THEN                      ! Read from file
        PRINT 700
        OPEN (31,FILE='input/ZPpar.dat',status='old')
        READ (31,*) gp
        READ (31,*) cLu
        READ (31,*) cRu
        READ (31,*) cLd
        READ (31,*) cRd
        READ (31,*) cLe
        READ (31,*) cRe
        READ (31,*) cLv
        READ (31,*) cRv
        READ (31,*) cLN
        READ (31,*) cRN
        CLOSE(31)
      ELSE IF (IZPMOD .EQ. 1) THEN                 ! Z'_lambda
        PRINT 710
        gp=SQRT(5d0/3d0)*g*SQRT(sw2)/cw
        Ynorm=2d0*SQRT(6d0)
        cLu=-1d0/Ynorm
        cRu=2d0/Ynorm
        cLd=-1d0/Ynorm
        cRd=-1d0/Ynorm
        cLe=0d0
        cRe=0d0
        cLv=0d0
        cRv=0d0
        cLN=-3d0/Ynorm
        cRN=3d0/Ynorm
      ELSE IF (IZPMOD .EQ. 2) THEN                 ! Z'_chi
        PRINT 720
        gp=SQRT(5d0/3d0)*g*SQRT(sw2)/cw
        Ynorm=2d0*SQRT(10d0)
        cLu=-1d0/Ynorm
        cRu=1d0/Ynorm
        cLd=-1d0/Ynorm
        cRd=-3d0/Ynorm
        cLe=3d0/Ynorm
        cRe=1d0/Ynorm
        cLv=3d0/Ynorm
        cRv=0d0
        cLN=-5d0/Ynorm
        cRN=5d0/Ynorm
      ELSE IF (IZPMOD .EQ. 3) THEN                 ! Z'_psi
        PRINT 730
        gp=SQRT(5d0/3d0)*g*SQRT(sw2)/cw
        Ynorm=2d0*SQRT(6d0)
        cLu=1d0/Ynorm
        cRu=-1d0/Ynorm
        cLd=1d0/Ynorm
        cRd=-1d0/Ynorm
        cLe=1d0/Ynorm
        cRe=-1d0/Ynorm
        cLv=1d0/Ynorm
        cRv=0d0
        cLN=1d0/Ynorm
        cRN=-1d0/Ynorm
      ELSE
        PRINT 790
        STOP
      ENDIF
!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)

!     Z boson

      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)

!     Higgs  -- not used

      GH=1d0

!     Heavy N

      r_Z = (MZ/mN)**2
      r_W = (MW/mN)**2
      r_H = (MH/mN)**2
      Vsq = VlN(1)**2 + VlN(2)**2 + VlN(3)**2

      GN_Wl = g**2/(64d0*pi)*Vsq*
     &  (mN**3/MW**2)*(1d0-r_W)*(1d0+r_W-2d0*r_W**2)
      IF (MW .GT. mN) GN_Wl=0d0
      IF (IMA .EQ. 1) GN_Wl=2d0*GN_Wl

      GN_Zv = g**2/(128d0*pi*cw**2)*Vsq*
     &  (mN**3/MZ**2)*(1d0-r_Z)*(1d0+r_Z-2d0*r_Z**2)
      IF (MZ .GT. mN) GN_Zv=0d0
      IF (IMA .EQ. 1) GN_Zv=2d0*GN_Zv

      GN_Hv = g**2/(128d0*pi)*Vsq*
     &  (mN**3/MW**2)*(1d0-r_H)**2
      IF (MH .GT. mN) GN_Hv=0d0
      IF (IMA .EQ. 1) GN_Hv=2d0*GN_Hv

      GN=GN_Wl+GN_Zv+GN_Hv

!     Z'

      GZp_uc=6d0*gp**2/(24d0*pi) * MZp * (cLu**2 + cRu**2)

      xt=mt/MZp
      GZp_t=3d0*gp**2/(24d0*pi) * MZp * SQRT(1d0-4d0*xt**2) *
     &  ( (cLu**2 + cRu**2) * (1d0-xt**2) + 6d0* cLu * cRu * xt**2 )
      IF (MZp .LT. 2d0*mt) GZp_t=0d0

      GZp_dsb=9d0*gp**2/(24d0*pi) * MZp * (cLd**2 + cRd**2)

      GZp_emt=3d0*gp**2/(24d0*pi) * MZp * (cLe**2 + cRe**2)

      GZp_v=3d0*gp**2/(24d0*pi) * MZp * (cLv**2 + cRv**2)
      
      xN=mN/mZp
      GZp_N=gp**2/(24d0*pi) * MZp * SQRT(1d0-4d0*xN**2) *
     &  ( (cLN**2 + cRN**2) * (1d0-xN**2) + 6d0* cLN * cRN * xN**2 )
      IF (IMA .EQ. 1) GZp_N=GZp_N/2d0
      IF (MZp .LT. 2d0*mN) GZp_N=0d0
c      GZp_N=3d0*GZp_N                                                 ! 3 N's

      GZp=GZp_uc+GZp_dsb+GZp_v+GZp_emt+GZp_t+GZp_N
      BRN=GZp_N/GZp

      PRINT 1050
      PRINT 1150,MZp,GZp
      PRINT 1160,GZp_uc/GZp/2d0,GZp_dsb/GZp/3d0,GZp_t/GZp
      PRINT 1170,GZp_emt/GZp/3d0,GZp_v/GZp/3d0,GZp_N/GZp
      PRINT 999
      PRINT 1100,mN,GN
      PRINT 1204,GN_Wl/GN,GN_Zv/GN,GN_Hv/GN
      PRINT 999
      PRINT 1201,GZ_nu/GZ,GZ_emt/GZ
      PRINT 1202,GZ_uc/GZ,GZ_dsb/GZ
      PRINT 999

!     Probabilities for each decay channel

      PW_T1=GN_Wl*DFLOAT(IDW1)*DFLOAT(IDLNC+IDLNV)/2d0
     &  +GN_Zv*DFLOAT(IDZ1)+GN_Hv*DFLOAT(IDH1)
      PRT1(1)=GN_Wl*DFLOAT(IDW1)*DFLOAT(IDLNC+IDLNV)/2d0/PW_T1
      PRT1(2)=GN_Zv*DFLOAT(IDZ1)/PW_T1
      PRT1(3)=GN_Hv*DFLOAT(IDH1)/PW_T1
      PW_T2=GN_Wl*DFLOAT(IDW2)*DFLOAT(IDLNC+IDLNV)/2d0
     &  +GN_Zv*DFLOAT(IDZ2)+GN_Hv*DFLOAT(IDH2)
      PRT2(1)=GN_Wl*DFLOAT(IDW2)*DFLOAT(IDLNC+IDLNV)/2d0/PW_T2
      PRT2(2)=GN_Zv*DFLOAT(IDZ2)/PW_T2
      PRT2(3)=GN_Hv*DFLOAT(IDH2)/PW_T2

      IF ((PW_T1 .EQ. 0d0) .OR. (PW_T2 .EQ. 0d0)) THEN
        PRINT 985
        PRINT 999
        STOP
      ENDIF

      BRT1ac(0)=0d0
      DO i=1,3
        BRT1ac(i)=BRT1ac(i-1)+PRT1(i)
        WTT1(i)=1d0/PRT1(i)
        IF (PRT1(i) .EQ. 0d0) WTT1(i)=0d0
      ENDDO
      IF (ABS(BRT1ac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRT1ac(3)
      BRT1ac(3)=1d0

      BRT2ac(0)=0d0
      DO i=1,3
        BRT2ac(i)=BRT2ac(i-1)+PRT2(i)
        WTT2(i)=1d0/PRT2(i)
        IF (PRT2(i) .EQ. 0d0) WTT2(i)=0d0
      ENDDO
      IF (ABS(BRT2ac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRT2ac(3)
      BRT2ac(3)=1d0

      PRZ(1)=GZ_nu/GZ
      PRZ(2)=GZ_emt/GZ
      PRZ(3)=GZ_uc/GZ
      PRZ(4)=GZ_dsb/GZ

      BRZac(0)=0d0
      DO i=1,4
        BRZac(i)=BRZac(i-1)+PRZ(i)
        WTZ(i)=1d0/PRZ(i)
      ENDDO
      IF (ABS(BRZac(4)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRZac(4)
      BRZac(4)=1d0

      PRFL(1)=VlN(1)**2/Vsq
      PRFL(2)=VlN(2)**2/Vsq
      PRFL(3)=VlN(3)**2/Vsq

      BRFLac(0)=0d0
      DO i=1,3
        BRFLac(i)=BRFLac(i-1)+PRFL(i)
        WTFL(i)=1d0/PRFL(i)
        IF (PRFL(i) .EQ. 0d0) WTFL(i)=0d0
      ENDDO
      IF (ABS(BRFLac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRFLac(3)
      BRFLac(3)=1d0

!     Include here colour factors and sum over fermions for Z, H

      WTZ(1)=WTZ(1)*3d0
      WTZ(2)=WTZ(2)*3d0
      WTZ(3)=WTZ(3)*6d0
      WTZ(4)=WTZ(4)*9d0

c      print *,prt1
c      print *,BRT1ac
c      print *,WTT1
c      print 999
c      print *,prt2
c      print *,BRT2ac
c      print *,WTT2
c      print 999
c      print *,BRZac
c      print *,WTZ
c      stop

      NGN=mN/GN*0.95d0
      NGW=MW/GW*0.95d0
      NGZ=MZ/GZ*0.95d0

!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO

      CALL SETPOL(istep,nhel8,ncomb8,8)
      DO i=1,MAXAMP
        DO j=1,ncomb8
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

      CALL SETPOL(istep,nhel6,ncomb6,6)
      CALL SETPOL(istep,nhel4,ncomb4,4)

c      DO i=1,ncomb8
c      PRINT *,(nhel8(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb8
c      DO i=1,ncomb6
c      PRINT *,(nhel6(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb6
c      DO i=1,ncomb4
c      PRINT *,(nhel4(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb4
c      STOP

      RETURN

500   FORMAT (' _____    _           _       ')
501   FORMAT ('|_   _|  (_)         | |      ')
502   FORMAT ('  | |_ __ _  __ _  __| | __ _ ')
503   FORMAT ('  | | \'__| |/ _` |/ _` |/ _` |')
504   FORMAT ('  | | |  | | (_| | (_| | (_| |')
505   FORMAT ('  \\_/_|  |_|\\__,_|\\__,_|\\__,_|')
700   FORMAT ('Z\' model from file')
710   FORMAT ('Model: Z\'_lambda')
720   FORMAT ('Model: Z\'_chi')
730   FORMAT ('Model: Z\'_psi')
790   FORMAT ('Z\' model not implemented')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
985   FORMAT ('Please open some decay channel for heavy leptons!')
999   FORMAT ('')
1000  FORMAT ('---------------------------------------')
1050  FORMAT ('Calculating Z\' -> NN production')
1100  FORMAT ('mN = ',F6.1,'   GN = ',F9.6)
1150  FORMAT ('MZ\' = ',F6.1,'   GZ\' = ',F5.2)      
1160  FORMAT ('Br(Z\' -> uu) = ',F5.4,'   Br(Z\' -> dd) = ',F5.4,
     &  '   Br(Z\' -> tt) = ',F5.4)
1170  FORMAT ('Br(Z\' -> ee) = ',F5.4,'   Br(Z\' -> vv) = ',F5.4,
     &  '   Br(Z\' -> NN) = ',F5.4)
1201  FORMAT ('BR(Z -> vv) = ',F5.3,'  BR(Z -> ll) = ',F5.3)
1202  FORMAT ('BR(Z -> uu) = ',F5.3,'  BR(Z -> dd) = ',F5.3)
1203  FORMAT ('BR(E -> Wv) = ',F5.3,
     &  '  BR(E -> Zl) = ',F5.3,'  BR(E -> Hl) = ',F5.3)
1204  FORMAT ('BR(N -> Wl) = ',F5.3,
     &  '  BR(N -> Zv) = ',F5.3,'  BR(N -> Hv) = ',F5.3)
1303  FORMAT ('Warning: in initialisation acc sum = ',D9.4)
      END


      SUBROUTINE SETPOL(nspin,hel,nhel,num)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel,num

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,num
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,num
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=num
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,num
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END

