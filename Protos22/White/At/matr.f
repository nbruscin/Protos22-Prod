      REAL*8 FUNCTION GQ_TA(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     P1 = g    P2 = q    P3 = t    P4 = gamma

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=5,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 Z3b(6),Z3c(6),Z3d(6),W7(6),W8(6),W9(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV
      REAL*8 lal,lar
      COMMON /tcoupF2/ lal,lar
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      REAL*8 MPH
      COMMON /test/ MPH

!     Couplings and other

      REAL*8 GWF(2),GG(2),GAuu(2),GAdd(2),GAee(2)
      COMPLEX*16 G2Atq(2),G2Aqt(2),G2Gtq(2),G2Gqt(2)
      REAL*8 F1MASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/
      DATA EIGEN_VEC(3,1) /-1d0/
      DATA EIGEN_VEC(4,1) /-1d0/
      DATA EIGEN_VEC(5,1) /-1d0/

      INCLUDE 'input/coupling.inc'

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)     ! g
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)      ! q
      CALL VXXXXX(P4,MPH,NHEL(6),1,W4)      ! gamma

!     top decay to W+ b

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)            ! nu
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)        ! e+
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)             ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)            ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)             ! t

      IF (IMOD .EQ. 1) THEN                         ! Photon anomalous

      CALL FVOXXX(W3,W1,GG,mt,Gt,W5)
      CALL IOVSmX(W2,W5,W4,G2Aqt,-1,AMP(1))
      CALL FVIXXX(W2,W1,GG,0d0,0d0,W6)
      CALL IOVSmX(W6,W3,W4,G2Aqt,-1,AMP(2))

!     Diagram with gluon splitting to b bbar

      CALL FVOXXX(W3c,W1,GG,mb,0d0,Z3c)
      CALL FVOXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t
      CALL IOVSmX(W2,W7,W4,G2Aqt,-1,AMP(3))
      AMP(4)=0d0
      AMP(5)=0d0

      ELSE                                         ! Gluon anomalous

      CALL FVIXXX(W2,W4,GAuu,0d0,0d0,W5)
      CALL IOVSmX(W5,W3,W1,G2Gqt,-1,AMP(1))
      CALL FVOXXX(W3,W4,GAuu,mt,Gt,W6)
      CALL IOVSmX(W2,W6,W1,G2Gqt,-1,AMP(2))

!     Diagrams with photon radiation off b, W and e

      CALL FVOXXX(W3c,W4,GAdd,mb,0d0,Z3c)          ! b radiating photon
      CALL FVOXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t
      CALL IOVSmX(W2,W7,W1,G2Gqt,-1,AMP(3))

      CALL JVVXXX(W3d,W4,e,MW,GW,Z3d)              ! W radiating photon
      CALL FVOXXX(W3c,Z3d,GWF,mt,Gt,W8)            ! t
      CALL IOVSmX(W2,W8,W1,G2Gqt,-1,AMP(4))

      CALL FVIXXX(W3b,W4,GAee,F1MASS,0d0,Z3b)      ! e+ radiating photon
      CALL JIOXXX(Z3b,W3a,GWF,MW,GW,Z3d)           ! W+
      CALL FVOXXX(W3c,Z3d,GWF,mt,Gt,W9)            ! t
      CALL IOVSmX(W2,W9,W1,G2Gqt,-1,AMP(5))

      ENDIF

      GQ_TA = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_TA =GQ_TA + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP)
      ENDDO
      END



      REAL*8 FUNCTION GQ_TBA(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     P1 = g    P2 = q~    P3 = t~    P4 = A

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=5,NEIGEN=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 Z3b(6),Z3c(6),Z3d(6),W7(6),W8(6),W9(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      INTEGER IMOD,IQFLAV
      COMMON /QFLAV/ IMOD,IQFLAV
      REAL*8 lal,lar
      COMMON /tcoupF2/ lal,lar
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      REAL*8 MPH
      COMMON /test/ MPH

!     Couplings and other

      REAL*8 GWF(2),GG(2),GAuu(2),GAdd(2),GAee(2)
      COMPLEX*16 G2Atq(2),G2Aqt(2),G2Gtq(2),G2Gqt(2)
      REAL*8 F1MASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /1d0/
      DATA EIGEN_VEC(2,1) /1d0/
      DATA EIGEN_VEC(3,1) /1d0/
      DATA EIGEN_VEC(4,1) /1d0/
      DATA EIGEN_VEC(5,1) /1d0/

      INCLUDE 'input/coupling.inc'

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)      ! g
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)      ! q~
      CALL VXXXXX(P4,MPH,NHEL(6),1,W4)       ! gamma

!     antitop decay to W- bbar

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)               ! nu~
      CALL OXXXXX(P3b,F1MASS,NHEL(4),1,W3b)             ! e-
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                ! W-
      CALL FVIXXX(W3c,W3d,GWF,mt,Gt,W3)                 ! t~

      IF (IMOD .EQ. 1) THEN                        ! Photon anomalous

      CALL FVIXXX(W3,W1,GG,mt,Gt,W5)
      CALL IOVSmX(W5,W2,W4,G2Atq,1,AMP(1))
      CALL FVOXXX(W2,W1,GG,0d0,0d0,W6)
      CALL IOVSmX(W3,W6,W4,G2Atq,1,AMP(2))

!     Diagram with gluon splitting to b bbar

      CALL FVIXXX(W3c,W1,GG,mb,0d0,Z3c)
      CALL FVIXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t~
      CALL IOVSmX(W7,W2,W4,G2Atq,1,AMP(3))
      AMP(4)=0d0
      AMP(5)=0d0

      ELSE                                         ! Gluon anomalous

      CALL FVOXXX(W2,W4,GAuu,0d0,0d0,W5)
      CALL IOVSmX(W3,W5,W1,G2Gtq,1,AMP(1))
      CALL FVIXXX(W3,W4,GAuu,mt,Gt,W6)
      CALL IOVSmX(W6,W2,W1,G2Gtq,1,AMP(2))

!     Diagrams with photon radiation off b, W and e

      CALL FVIXXX(W3c,W4,GAdd,mb,0d0,Z3c)          ! b~ radiating photon
      CALL FVIXXX(Z3c,W3d,GWF,mt,Gt,W7)            ! t~
      CALL IOVSmX(W7,W2,W1,G2Gtq,1,AMP(3))

      CALL JVVXXX(W4,W3d,e,MW,GW,Z3d)              ! W radiating photon
      CALL FVIXXX(W3c,Z3d,GWF,mt,Gt,W8)            ! t~
      CALL IOVSmX(W8,W2,W1,G2Gtq,1,AMP(4))

      CALL FVOXXX(W3b,W4,GAee,F1MASS,0d0,Z3b)      ! e- radiating photon
      CALL JIOXXX(W3a,Z3b,GWF,MW,GW,Z3d)           ! W-
      CALL FVIXXX(W3c,Z3d,GWF,mt,Gt,W9)            ! t~
      CALL IOVSmX(W9,W2,W1,G2Gtq,1,AMP(5))

      ENDIF

      GQ_TBA = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_TBA =GQ_TBA + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END
