      REAL*8 FUNCTION GQ_TH(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     P1 = g    P2 = q    P3 = t    P4 = H

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=5)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 etal,etar
      COMMON /tcoupF4/ etal,etar
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GHtq(2),GHqt(2)
      REAL*8 F1MASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /-1d0/
      DATA EIGEN_VEC(2,1) /-1d0/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GHtq(1)=-etal/SQRT(2d0)          ! t in q out: top decay
      GHtq(2)=-etar/SQRT(2d0)
      GHqt(1)=CONJG(GHtq(2))           ! t out q in: antitop decay
      GHqt(2)=CONJG(GHtq(1))

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)     ! g
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)      ! q
      CALL SXXXXX(P4,1,W4)                  ! H

!     top decay to W+ b

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)            ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)        ! e+ / qbar
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)             ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)            ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)             ! t

      CALL FVOXXX(W3,W1,GG,mt,Gt,W5)
      CALL IOSXXX(W2,W5,W4,GHqt,AMP(1))

      CALL FVIXXX(W2,W1,GG,0d0,0d0,W6)
      CALL IOSXXX(W6,W3,W4,GHqt,AMP(2))

      GQ_TH = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_TH =GQ_TH + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP)
      ENDDO
      END



      REAL*8 FUNCTION GQ_TBH(P1,P2,P3a,P3b,P3c,P4,NHEL)

!     P1 = g    P2 = q~    P3 = t~    P4 = H

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=2,NEIGEN=1,NEXTERNAL=5)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6),W6(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 etal,etar
      COMMON /tcoupF4/ etal,etar
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GHtq(2),GHqt(2)
      REAL*8 F1MASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /1d0/
      DATA EIGEN_VEC(2,1) /1d0/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GHtq(1)=-etal/SQRT(2d0)          ! t in q out: top decay
      GHtq(2)=-etar/SQRT(2d0)
      GHqt(1)=CONJG(GHtq(2))           ! t out q in: antitop decay
      GHqt(2)=CONJG(GHtq(1))

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)      ! g
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)      ! q~
      CALL SXXXXX(P4,1,W4)                   ! H

!     antitop decay to W- bbar

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)               ! nu~ / qbar
      CALL OXXXXX(P3b,F1MASS,NHEL(4),1,W3b)             ! e-  / q
      CALL IXXXXX(P3c,mb,NHEL(5),-1,W3c)                ! b~
      CALL JIOXXX(W3a,W3b,GWF,MW,GW,W3d)                ! W-
      CALL FVIXXX(W3c,W3d,GWF,mt,Gt,W3)                 ! t~

      CALL FVIXXX(W3,W1,GG,mt,Gt,W5)
      CALL IOSXXX(W5,W2,W4,GHtq,AMP(1))

      CALL FVOXXX(W2,W1,GG,0d0,0d0,W6)
      CALL IOSXXX(W3,W6,W4,GHtq,AMP(2))

      GQ_TBH = 0d0
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_TBH =GQ_TBH + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END
