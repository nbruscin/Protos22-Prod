      SUBROUTINE PROTOS_INIT
      IMPLICIT NONE

!     --------------------------------------------------------------------
!     Process codes used
!
!     Red:     3 = tj     4 = tbj    5 = tb    6 = tW   7 = tWb   8 = tt
!     White:   9 = Zqt   10 = Zt    11 = Aqt  12 = At  13 = gqt  14 = t
!             15 = Hqt   16 = Ht    17 = llqt 18 = tt
!     Rose:   51 = TT    52 = BB    53 = XX   54 = YY
!             61 = Tj    62 = Tbj   63 = Bj   64 = Bbj  65 = Yj   66 = Ybj
!             67 = Ttj   69 = Xtj
!     Triada: 31 = EE    32 = EN    33 = NN   36 = Zp   38 = Wp
!             41 = D2D2  42 = D2D1  43 = D1D1
!     --------------------------------------------------------------------

!     External variables

      INTEGER LONGITUD

!     User process initialization commonblock.

      INTEGER MAXPUP
      PARAMETER (MAXPUP=100)
      INTEGER IDBMUP,PDFGUP,PDFSUP,IDWTUP,NPRUP,LPRUP
      DOUBLE PRECISION EBMUP,XSECUP,XERRUP,XMAXUP
      COMMON/HEPRUP/IDBMUP(2),EBMUP(2),PDFGUP(2),PDFSUP(2),
     &IDWTUP,NPRUP,XSECUP(MAXPUP),XERRUP(MAXPUP),XMAXUP(MAXPUP),
     &LPRUP(MAXPUP)
      SAVE /HEPRUP/

!     Output

      INTEGER lun,IHRD
      COMMON /PRdata/ lun,IHRD
      REAL*8 PTbmax,IMATCH
      COMMON /PRmatch/ PTbmax,IMATCH

!     Local: filename

      CHARACTER*100 procname
      CHARACTER*7 dir
      INTEGER l

!     Local

      CHARACTER*100 string
      INTEGER i,idum1,idum2
      REAL*8 ET
      REAL*8 mt,Gt,mb,MH                    ! For Red and White
      REAL*8 mL,GE,GN,mX,GX,mD,GD           ! For Triada
      REAL*8 mQ,GQ,Vmix                     ! For Rose

!     ----------------------------------------------------

      lun=35                ! Select here some number not already used
      dir='PROTOS/'

!     Read protos.dat

      OPEN (lun,file='protos.dat')
      READ (lun,*) procname
      l=LONGITUD(procname)
      CLOSE(lun)

!     Open and read summary data

      OPEN (lun,file=dir//procname(1:l)//'_unw.par',status='old')
      READ (lun,*) IHRD
      READ (lun,*) ET

!     Set p pbar and CM energy

      IDBMUP(1)=2212
      IDBMUP(2)=2212
      EBMUP(1)=ET/2d0
      EBMUP(2)=ET/2d0

!     Masses

      IF ((IHRD .GE. 3) .AND. (IHRD .LE. 18)) THEN
        READ (lun,*) mt,Gt,mb,MH
      ELSE IF ((IHRD .GE. 31) .AND. (IHRD .LE. 33)) THEN
        READ (lun,*) mL,GE,GN,MH
      ELSE IF ((IHRD .GE. 34) .AND. (IHRD .LE. 38)) THEN
        READ (lun,*) mX,GX,idum1,idum2
        READ (lun,*) mL,GE,GN,MH
      ELSE IF ((IHRD .GE. 41) .AND. (IHRD .LE. 43)) THEN
        READ (lun,*) mD,GD
      ELSE IF ((IHRD .GE. 51) .AND. (IHRD .LE. 69)) THEN
        READ (lun,*) mt,Gt,mb,MH
        READ (lun,*) mQ,GQ,Vmix
      ELSE
        PRINT 10001,IHRD
        STOP
      ENDIF

      IF ((IHRD .GE. 3) .AND. (IHRD .LE. 18)) THEN

      WRITE (string,'(F6.2)') mt
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(6,1)='//string(1:i))
      WRITE (string,'(F6.2)') Gt
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(6,2)='//string(1:i))
      WRITE (string,'(F6.2)') mb
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(5,1)='//string(1:i))
      WRITE (string,'(F6.2)') MH
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(25,1)='//string(1:i))

      ELSE IF ((IHRD .GE. 31) .AND. (IHRD .LE. 33)) THEN

      WRITE (string,'(F7.2)') mL
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(17,1)='//string(1:i))
      CALL PYGIVE('PMAS(18,1)='//string(1:i))
      WRITE (string,'(F10.6)') GE
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(17,2)='//string(1:i))
      WRITE (string,'(F10.6)') GN
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(18,2)='//string(1:i))
      WRITE (string,'(F6.2)') MH
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(25,1)='//string(1:i))

      ELSE IF ((IHRD .GE. 34) .AND. (IHRD .LE. 36)) THEN

      WRITE (string,'(F7.1)') mX
      i=LONGITUD(string)
      IF (IHRD .NE. 35) THEN
        CALL PYGIVE('PMAS(32,1)='//string(1:i))
      ELSE
        CALL PYGIVE('PMAS(34,1)='//string(1:i))
      ENDIF
      WRITE (string,'(F6.2)') GX
      i=LONGITUD(string)
      IF (IHRD .NE. 35) THEN
        CALL PYGIVE('PMAS(32,2)='//string(1:i))
      ELSE
        CALL PYGIVE('PMAS(34,2)='//string(1:i))
      ENDIF
      WRITE (string,'(F7.2)') mL
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(17,1)='//string(1:i))
      CALL PYGIVE('PMAS(18,1)='//string(1:i))
      WRITE (string,'(F10.6)') GE
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(17,2)='//string(1:i))
      WRITE (string,'(F10.6)') GN
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(18,2)='//string(1:i))
      WRITE (string,'(F6.2)') MH
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(25,1)='//string(1:i))

      ELSE IF (IHRD .EQ. 38) THEN

      WRITE (string,'(F7.1)') mX
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(34,1)='//string(1:i))
      WRITE (string,'(F6.2)') GX
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(34,2)='//string(1:i))
      WRITE (string,'(F7.2)') mL
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(17,1)='//string(1:i))
      CALL PYGIVE('PMAS(18,1)='//string(1:i))
      WRITE (string,'(F10.6)') GE
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(17,2)='//string(1:i))
      WRITE (string,'(F10.6)') GN
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(18,2)='//string(1:i))
      WRITE (string,'(F6.2)') MH
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(25,1)='//string(1:i))

      ELSE IF ((IHRD .GE. 41) .AND. (IHRD .LE. 43)) THEN

c      WRITE (string,'(F7.1)') mD
c      i=LONGITUD(string)
c      CALL PYGIVE('PMAS(37,1)='//string(1:i))
c      CALL PYGIVE('PMAS(9900041,1)='//string(1:i))
c      WRITE (string,'(F10.6)') GD
c      i=LONGITUD(string)
c      CALL PYGIVE('PMAS(37,2)='//string(1:i))
c      CALL PYGIVE('PMAS(9900041,2)='//string(1:i))

      ELSE IF ((IHRD .GE. 51) .AND. (IHRD .LE. 69)) THEN

      WRITE (string,'(F6.2)') mt
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(6,1)='//string(1:i))
      WRITE (string,'(F6.2)') Gt
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(6,2)='//string(1:i))
      WRITE (string,'(F6.2)') mb
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(5,1)='//string(1:i))
      WRITE (string,'(F6.2)') MH
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(25,1)='//string(1:i))

      WRITE (string,'(F6.2)') mQ
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(8,1)='//string(1:i))
      CALL PYGIVE('PMAS(7,1)='//string(1:i))
      WRITE (string,'(F6.2)') GQ
      i=LONGITUD(string)
      CALL PYGIVE('PMAS(8,2)='//string(1:i))
      CALL PYGIVE('PMAS(7,2)='//string(1:i))

      ENDIF

!     Read matching parameters, if any

      IF ( (IHRD .EQ. 3) .OR. (IHRD .EQ. 61) .OR. (IHRD .EQ. 63)
     &  .OR. (IHRD .EQ. 65) ) THEN
        READ (lun,*) IMATCH,PTbmax
        IF (IMATCH .EQ. 2) CALL PYGIVE('MSTP(143)=1')
      ELSE
        IMATCH=0
      ENDIF

100   CLOSE (lun)


!     Open event file

      OPEN (lun,file=dir//procname(1:l)//'.unw',status='old')
      RETURN

10001 FORMAT ('Unknown process IHRD = ',I2)

      END





      SUBROUTINE PROTOS_READ
      IMPLICIT NONE

!     User process event common block.

      INTEGER MAXNUP
      PARAMETER (MAXNUP=500)
      INTEGER NUP,IDPRUP,IDUP,ISTUP,MOTHUP,ICOLUP
      DOUBLE PRECISION XWGTUP,SCALUP,AQEDUP,AQCDUP,PUP,VTIMUP,SPINUP
      COMMON/HEPEUP/NUP,IDPRUP,XWGTUP,SCALUP,AQEDUP,AQCDUP,IDUP(MAXNUP),
     &ISTUP(MAXNUP),MOTHUP(2,MAXNUP),ICOLUP(2,MAXNUP),PUP(5,MAXNUP),
     &VTIMUP(MAXNUP),SPINUP(MAXNUP)
      SAVE /HEPEUP/

!     Standard PYTHIA commonblocks.

      INTEGER KCHG
      REAL*8 PMAS,PARF,VCKM
      COMMON/PYDAT2/KCHG(500,4),PMAS(500,4),PARF(2000),VCKM(4,4)

!     Data from PROTOS_INIT

      INTEGER lun,IHRD
      COMMON /PRdata/ lun,IHRD

!     Some output

      INTEGER ISIGNWT
      COMMON /PRinfo/ ISIGNWT

!     Local

      INTEGER NJET,CHVB,CHL
      REAL*8 Pz,mf
      REAL*8 mtau,mb,mt,MW,MZ,MH,mL,MZp,MWp,mQ

!     Dummy

      INTEGER i0,i,j,id_f1,id_f2,id_VB,id_q,id_l,id_b1,id_b2,nn1
      INTEGER IDB1,IDB2,id_L1,id_L2,CHL1,CHL2
      INTEGER ID_HQ,id_Qt,num_Qt,id_b

!     ----------------------------------------------------

!     Clear arrays

      DO i=1,MAXNUP
        MOTHUP(1,i)=0
        MOTHUP(2,i)=0
        ICOLUP(1,i)=0
        ICOLUP(2,i)=0
        SPINUP(I)=9d0
        PUP(1,i)=0d0    
        PUP(2,i)=0d0    
        PUP(5,i)=0d0
        VTIMUP(i)=1d0
      ENDDO

      IDPRUP=661

      mtau=PMAS(15,1)
      mb=PMAS(5,1)
      mt=PMAS(6,1)
      MW=PMAS(24,1)
      MZ=PMAS(23,1)
      MH=PMAS(25,1)
      mL=PMAS(17,1)
      MZp=PMAS(32,1)
      MWp=PMAS(34,1)
      mQ=PMAS(8,1)

!     ====================
!     Select process group
!     ====================

!     One top quark (plus jets, a photon or a Higgs)

      IF ( ((IHRD .GE. 3) .AND. (IHRD .LE. 5)) .OR. (IHRD .EQ. 12)
     &  .OR. (IHRD .EQ. 14) .OR. (IHRD .EQ. 16))
     &  GOTO 1003

!     One top quark and a W/Z boson (plus jets)

      IF ((IHRD .EQ. 6) .OR. (IHRD .EQ. 7) .OR. (IHRD .EQ. 10))
     &  GOTO 1006

!     A top quark pair decaying both to W/Z

      IF ((IHRD .EQ. 8) .OR. (IHRD .EQ. 9) .OR. (IHRD .EQ. 18))
     &   GOTO 1008

!     A top quark pair decaying to W/Z and a photon/gluon/Higgs

      IF ((IHRD .EQ. 11) .OR. (IHRD .EQ. 13) .OR. (IHRD .EQ. 15))
     &  GOTO 1011

!     A top quark pair decaying to W/Z and 3-body

      IF (IHRD .EQ. 17) GOTO 1017

!     Two heavy leptons

      IF ((IHRD .GE. 31) .AND. (IHRD .LE. 36))
     &  GOTO 1031

!     One heavy lepton

      IF ((IHRD .GE. 37) .AND. (IHRD .LE. 38))
     &  GOTO 1037

!     Two scalar triplets decaying into four leptons

      IF ((IHRD .GE. 41) .AND. (IHRD .LE. 43))
     &  GOTO 1041

!     T Tbar

      IF (IHRD .EQ. 51) GOTO 1051

!     B Bbar

      IF (IHRD .EQ. 52) GOTO 1052

!     X Xbar

      IF (IHRD .EQ. 53) GOTO 1053

!     Y Ybar

      IF (IHRD .EQ. 54) GOTO 1054

!     One T quark plus j, bj, t j

      IF ((IHRD .EQ. 61) .OR. (IHRD .EQ. 62) .OR. (IHRD .EQ. 67))
     &  GOTO 1061

!     One B quark plus j, bj

      IF ((IHRD .EQ. 63) .OR. (IHRD .EQ. 64)) GOTO 1063

!     One Y quark plus j, bj

      IF ((IHRD .EQ. 65) .OR. (IHRD .EQ. 66)) GOTO 1065
 
 !    Xtj

      IF (IHRD .EQ. 69) GOTO 1069
     
!     None of the above

      PRINT 10001,IHRD
      STOP

!     ------------------------------------------------------
!     Read tj (3), tbj (4), tb (5), At (12), t (14), Ht (16)
!     ------------------------------------------------------

      include 'formats/tj.inc'

!     -----------------------------
!     Read tW (6), tWb (7), Zt (10)
!     -----------------------------

      include 'formats/tW.inc'

!     --------------------
!     Read tt (8), Zqt (9)
!     --------------------

      include 'formats/tt.inc'

!     ---------------------------------
!     Read Aqt (11), gqt (13), Hqt (15)
!     ---------------------------------

      include 'formats/tfcn.inc'

!     --------------
!     Read llqt (17)
!     --------------

      include 'formats/tt4f.inc'

!     ---------------------------------------------------------------------------
!     Read E+E- (31), EN (32), NN (33), Z'-> E+E- (34), W'-> EN (35), Z'-> NN(36)  
!     ---------------------------------------------------------------------------

      include 'formats/2HL.inc'

!     --------------------------
!     Read lN (37), W'-> lN (38)
!     --------------------------

      include 'formats/1HL.inc'

!     --------------------------------------------------
!     Read D++ D-- (41), D++D- / D+ D-- (42), D+ D- (43)
!     --------------------------------------------------

      include 'formats/DD4l.inc'

!     ------------
!     Read TT (51)
!     ------------

      include 'formats/TT.inc'

!     ------------
!     Read BB (52)
!     ------------

      include 'formats/BB.inc'

!     ------------
!     Read XX (53)
!     ------------

      include 'formats/XX.inc'

!     ------------
!     Read YY (54)
!     ------------

      include 'formats/YY.inc'

!     --------------------------------
!     Read Tj (61), Tbj (62), Ttj (67)
!     --------------------------------

      include 'formats/Tj.inc'

!     ----------------------
!     Read Bj (63), Bbj (64)
!     ----------------------

      include 'formats/Bj.inc'

!     ----------------------
!     Read Yj (65), Ybj (66)
!     ----------------------

      include 'formats/Yj.inc'

!     -------------
!     Read Xtj (69)
!     -------------

      include 'formats/Xtj.inc'


10001 FORMAT ('Unknown process IHRD = ',I2)
10002 FORMAT ('Rewinding ',I2)

      END



      SUBROUTINE PROTOS_VETO(IVETO)
      IMPLICIT NONE

!     Arguments

      INTEGER IVETO

!     Data from PROTOS_INIT

      INTEGER lun,IHRD
      COMMON /PRdata/ lun,IHRD
      REAL*8 PTbmax,IMATCH
      COMMON /PRmatch/ PTbmax,IMATCH

!     User process event common block.

      INTEGER MAXNUP
      PARAMETER (MAXNUP=500)
      INTEGER NUP,IDPRUP,IDUP,ISTUP,MOTHUP,ICOLUP
      DOUBLE PRECISION XWGTUP,SCALUP,AQEDUP,AQCDUP,PUP,VTIMUP,SPINUP
      COMMON/HEPEUP/NUP,IDPRUP,XWGTUP,SCALUP,AQEDUP,AQCDUP,IDUP(MAXNUP),
     &ISTUP(MAXNUP),MOTHUP(2,MAXNUP),ICOLUP(2,MAXNUP),PUP(5,MAXNUP),
     &VTIMUP(MAXNUP),SPINUP(MAXNUP)

!     Event record

      INTEGER N,NPAD,K
      REAL*8 P,V
      COMMON/PYJETS/N,NPAD,K(10000,5),P(10000,5),V(10000,5)

!     Local

      INTEGER IDB,ib
      REAL*8 Pb(0:3),PTb

!     ----------------------------------------------------

      IVETO=0
      IF (IMATCH .NE. 2) RETURN

      IDB=IDUP(2)      
      ib=1

      DO WHILE ((K(ib,2) .NE. -IDB) .OR. (K(ib,1) .NE. 3))
        ib=ib+1
        IF (ib .GT. N) RETURN
      ENDDO
      
      Pb(0) = P(ib,4)
      Pb(1) = P(ib,1)
      Pb(2) = P(ib,2)
      Pb(3) = P(ib,3)

      PTb=SQRT(Pb(1)**2+Pb(2)**2)
      IF (PTb .GT. PTbmax) IVETO = 1

      RETURN

      END



      INTEGER FUNCTION LONGITUD(name)
      CHARACTER*100 name
      INTEGER i
      i = LEN(name)
      DO WHILE (name(i:i) .EQ. ' ')
        i = i - 1
      ENDDO
      LONGITUD = i
      RETURN
      END
