import os, sys, getopt, string, time, commands, subprocess
#from optparse import OptionParser, OptionGroup
# =================================================================================
#  put
# =================================================================================
def put(text):
    # print '%s' % text
    myscript.write('%s\n' % text)

# =================================================================================
#  br
# =================================================================================
def br():
    # print
    myscript.write('\n')

#=====================================================================
# main()
#=====================================================================
def main():
    
    # globals
    global workingDir
    workingDir = os.environ['PWD']

    # retrieve the ROOT architecture
    _retrieveSystemConfiguration()

    # LHAPDF library (https://www.hepforge.org/downloads/lhapdf)
    # Note that a compiler supporting C++11 is required (gcc 4.8.1 was the first feature-complete implementation of the 2011 C++ standard)
    LHAPDFver = 'LHAPDF-6.2.1'
    # if int(gccVer.replace('.','')) < 481 and OStype=="Linux": LHAPDFver = 'lhapdf-5.9.1'
    
    if LHAPDFver.startswith('LHAPDF-6'): PDFset = [ "CTEQ6L1", "cteq6l1", "10042" ]
    else: PDFset = [ "CTEQ6L1", "cteq6ll.LHpdf", "10042" ]

    # prepare environment to use the lhapdf library
    _LHAPDF_precompiled_package(LHAPDFver, PDFset)

    print os.environ["LHAPDFLIBDIR"]

#    os.chdir("Protos22")
#    os.system("rm -f extlib.mk")
#    os.system("echo -n \"EXTLIB = -L%s -lLHAPDF\" > extlib.mk" % os.environ["LHAPDFLIBDIR"])
#    os.system("more extlib.mk")
#    os.chdir(workingDir)
    print

#    scriptname = "LHAPDF.sh"

#    global myscript
#    myscript = open(scriptname, "w")
#    put('#!/bin/bash')
#    br()
#    put('export LHAPDFSYS=%s' % os.environ["LHAPDFLIBDIR"])
#    #put('export LD_LIBRARY_PATH=${LHAPDFSYS}')
#    put('export LD_LIBRARY_PATH=${LHAPDFSYS}:${LD_LIBRARY_PATH}')
#    put('make toAtlas')
#    put('make clean; make all')

#export LHAPDFSYS=${LHAPDFPATH}/${LHAPDFver}_${systemConfig}/lib
#export LHAPATH=${LHAPDFPATH}/PDFsets
#export LHAPDFINCDIR=${LHAPDFPATH}/${LHAPDFver}_${systemConfig}/include
#export LHAPDFLIBDIR=${LHAPDFPATH}/${LHAPDFver}_${systemConfig}/lib
#export LD_LIBRARY_PATH=${LHAPDFSYS}
#export LD_LIBRARY_PATH=${LHAPDFSYS}:${LD_LIBRARY_PATH}

#    myscript.close()

#    os.system('chmod a+x %s' % scriptname)
#    os.system('source %s/%s' % (workingDir, scriptname))
#    os.system("make all")

#    os.system('rm -f %s' % scriptname)

# ====================================================================
#  _LHAPDF_precompiled_package
# ===================================================================
def _LHAPDF_precompiled_package(LHAPDFver, PDFset):

    # make sure that the libLHAPDF directory exists
    os.system('mkdir -p %s/libLHAPDF' % workingDir)

    if not LHAPDFver.startswith('LHAPDF-6'): os.system('mkdir -p libLHAPDF/PDFsets/')
    if os.path.isdir("%s/libLHAPDF/%s_%s/" % (workingDir, LHAPDFver, SystemConfig)):
        print chr(27)+"[0;35m"+("Checking the LHAPDF library (%s_%s)..." % (LHAPDFver, SystemConfig))+chr(27)+"[0m"
        if not os.path.isfile("%s/libLHAPDF/%s_%s/bin/lhapdf-config" % (workingDir, LHAPDFver, SystemConfig)):
            errmsg = "Problem found with the preparing the precompiled LHAPDF library (%s_%s.tar.gz)... trying again!" % (LHAPDFver, SystemConfig)
            os.system('rm -rf %s/libLHAPDF/%s_%s' %  (workingDir, LHAPDFver, SystemConfig))
            print chr(27)+"[1;31m"+errmsg+chr(27)+"[0m"
            return _LHAPDF_precompiled_package()
        print ' - %s_%s is OK!' % (LHAPDFver, SystemConfig)
        # os.system('rm -rf %s/libLHAPDF/%s_%s' %  (workingDir, LHAPDFver, SystemConfig))
    else:
        print chr(27)+"[0;35m"+("Precomping the LHAPDF library: libLHAPDF/%s_%s" % (LHAPDFver, SystemConfig))+chr(27)+"[0m"
        os.chdir('%s/libLHAPDF' % workingDir)
        # if os.path.isfile("%s/libLHAPDF/%s.tar.gz" % (workingDir, LHAPDFver)): os.system("rm %s/libLHAPDF/%s.tar.gz" % (workingDir, LHAPDFver))
        print chr(27)+"[0;35m"+("- Downloading http://www.hepforge.org/archive/lhapdf/%s.tar.gz..." % LHAPDFver)+chr(27)+"[0m"
        if 'macos64' in SystemConfig: os.system('curl -L "http://www.hepforge.org/archive/lhapdf/%s.tar.gz" -o "%s.tar.gz"' % (LHAPDFver, LHAPDFver))
        else: os.system('wget --quiet http://www.hepforge.org/archive/lhapdf/%s.tar.gz' % LHAPDFver)
        print '... done!'
        print chr(27)+"[0;35m"+("- Compiling the LHAPDF library (%s_%s)" % (LHAPDFver, SystemConfig))+chr(27)+"[0m"
        os.system('tar xzf %s.tar.gz' % LHAPDFver)
        # remove any previous version and create a clean new directory
        os.system('rm -rf %s/libLHAPDF/%s_%s' %  (workingDir, LHAPDFver, SystemConfig))
        os.system('mkdir -p %s/libLHAPDF/%s_%s' %  (workingDir, LHAPDFver, SystemConfig))
        os.chdir("%s/libLHAPDF/%s" % (workingDir, LHAPDFver))
        if 'macos64' in SystemConfig: os.system('./configure --prefix=%s/libLHAPDF/%s_%s CXX=clang++' % (workingDir, LHAPDFver, SystemConfig))
        else: os.system('./configure --prefix=%s/libLHAPDF/%s_%s' % (workingDir, LHAPDFver, SystemConfig))
        os.system('make -j6 && make install')
        os.chdir('%s/libLHAPDF' % workingDir)
        os.system('rm -rf %s; rm -f %s.tar.gz*' %  (LHAPDFver, LHAPDFver))
        os.system('dir')
        # os.system('tar -cf %s_%s.tar %s_%s' %  (LHAPDFver, SystemConfig, LHAPDFver, SystemConfig))
        # os.system('gzip %s_%s.tar' %  (LHAPDFver, SystemConfig))
        # os.system('rm -rf %s_%s' %  (LHAPDFver, SystemConfig))
        # print 'rm -rf %s_%s' %  (LHAPDFver, SystemConfig)
    print " - Precompiled LHAPDF library is now ready here: libLHAPDF/%s_%s" % (LHAPDFver, SystemConfig)

    # ser environment for LHAPDF
    _LHAPDFLib_setenv(LHAPDFver)

    # get the requested PDF sets
    _get_PDFset(LHAPDFver, PDFset)

    if LHAPDFver.startswith('LHAPDF-6'):
        os.chdir('%s/libLHAPDF' % workingDir)
        os.system('rm -f %s_%s.tar.gz' %  (LHAPDFver, SystemConfig))
        os.system('tar -cf %s_%s.tar %s_%s' %  (LHAPDFver, SystemConfig, LHAPDFver, SystemConfig))
        os.system('gzip %s_%s.tar' %  (LHAPDFver, SystemConfig))
        os.chdir(workingDir)

# ====================================================================
#  _LHAPDFLib_setenv
# ====================================================================
def _LHAPDFLib_setenv(LHAPDFver):
    # set environment for LHAPDF library

    if LHAPDFver.startswith('LHAPDF-6'): os.environ["LHAPATH"] = "%s/libLHAPDF/%s_%s/share/LHAPDF" % (workingDir, LHAPDFver, SystemConfig)
    else: os.environ["LHAPATH"] = "%s/libLHAPDF/PDFsets" % (workingDir)
    os.environ["LHAPDFINCDIR"] = "%s/libLHAPDF/%s_%s/include" % (workingDir, LHAPDFver, SystemConfig)
    os.environ["LHAPDFLIBDIR"] = "%s/libLHAPDF/%s_%s/lib" % (workingDir, LHAPDFver, SystemConfig)
    os.environ["LHAPDFSYS"] = os.environ["LHAPDFLIBDIR"]

    print ' - LHAPATH = ' + os.environ["LHAPATH"]
    print ' - LHAPDFINCDIR = ' + os.environ["LHAPDFINCDIR"]
    print ' - LHAPDFLIBDIR = ' + os.environ["LHAPDFLIBDIR"]
    print ' - LHAPDFSYS = ' + os.environ["LHAPDFSYS"]

    if os.getenv("LD_LIBRARY_PATH") is None: os.environ["LD_LIBRARY_PATH"] = ""
    os.environ["LD_LIBRARY_PATH"] = os.environ["LHAPDFLIBDIR"]+":"+os.environ["LD_LIBRARY_PATH"]
    print ' - LD_LIBRARY_PATH = ' + os.environ["LD_LIBRARY_PATH"]

# ====================================================================
#  _get_PDFset
# ====================================================================
def _get_PDFset(LHAPDFver, PDFset):
    # print PDFset
    if len(PDFset[0])!=0:
        if os.getenv("LHAPATH") is not None and os.getenv("LHAPDFINCDIR") is not None:
            if os.path.isfile(os.getenv("LHAPATH")+'/'+PDFset[1]): return
            if LHAPDFver.startswith('LHAPDF-6'):
                if not os.path.isdir(os.getenv("LHAPATH")+'/'+PDFset[1]):
                    print ' - Downloading '+os.getenv("LHAPATH")+'/'+PDFset[1]+'...'
                    pdfsetspath = 'https://www.hepforge.org/archive/lhapdf/pdfsets'
                    if 'macos64' in SystemConfig: os.system('curl -L "%s/%s/%s.tar.gz" -o "%s/%s.tar.gz"' % (pdfsetspath, LHAPDFver.split('-')[1], PDFset[1], os.environ["LHAPATH"], PDFset[1]))
                    else: os.system('wget %s/%s/%s.tar.gz -O- | tar xz -C %s/' % (pdfsetspath, LHAPDFver.split('-')[1], PDFset[1], os.environ["LHAPATH"]))
                else:
                    print ' - PDF sets: '+os.getenv("LHAPATH")+'/'+PDFset[1]+', already exist!'
            else:
                command  = os.getenv("LHAPDFINCDIR")+'/../bin/lhapdf-getdata '+PDFset[1]
                command += ' --dest='+os.getenv("LHAPATH")+'/'
                print chr(27)+"[0;35m"+("Retrieving the requested PDF set... %s" % PDFset[1])+chr(27)+"[0m"
                os.system(command)
            print

# ====================================================================
#  _retrieveSystemConfiguration
# ====================================================================
def _retrieveSystemConfiguration():

    global SystemConfig
    OStype = commands.getoutput("uname")
    OSarch = commands.getoutput("uname -m")
    gccVer = commands.getoutput("gcc -dumpversion")

    # system configuration
    if OStype=="Linux":
        # https://information-technology.web.cern.ch/book/lxplus-service/lxplus-guide/lxplus-aliases
        if OSarch=="x86_64":
            SystemConfig = "x86_64-slc6-gcc" + gccVer.replace('.','')[:2] + "-opt"
        else:
            print "Linux architecture not supported!"
            sys.exit()        
    elif OStype=="Darwin":
        for i in commands.getoutput("clang -v").splitlines():
            if 'LLVM version' in i: clangVer = (i.replace('Apple LLVM version ','')).split(' ')[0]
        # print 'clang version:' + clangVer
        if OSarch=="x86_64":
            SystemConfig = "x86_64_macos64-clang"+clangVer.replace('.','')[:2]
        else:
            print "macOS architecture not supported!"
            sys.exit()
    else:
        print "OS not supported!"
        sys.exit()

# ====================================================================
#  __main__
# ====================================================================
if __name__ == "__main__":
    main()
