#include "Protos.h"
#include <iostream>
#include <iomanip>
#include <cstdio>

int main(int argc, char **argv) {
  const std::string program=argv[0];
  const std::string usage = program + " inputFile  [firstEvent] [nEvents]"; 

  //
  // Print usage statement if no arguments:
  //
  if (argc==1) {
    std::cerr << usage << std::endl;
    exit (0);
  }
  //
  // Open the input file:
  //
  FILE *inputFile=fopen(argv[1],"r");
  if (!inputFile) {
    std::cerr << "Error openining input file; exiting" << std::endl;
  }
  ProtosEvent event;
  //
  // Set parameter start and nEvents:
  //
  unsigned int start   = argc>2 ? atoi(argv[2]):1;
  unsigned int nEvents = argc>3 ? atoi(argv[3]):0;

  std::cout << std::scientific << std::setprecision(8);
  fseek(inputFile,(start-1)*sizeof(ProtosEvent),SEEK_SET);
  unsigned int iEvent=1;

  while (fread(&event, sizeof(ProtosEvent), 1, inputFile)) {
    // Reset the event number:
    event.eventNumber = (start-1)+iEvent;

    /*
    std::cout << "         "
	      << event.eventNumber << " " 
	      << event.weight << " " 
	      << event.Q << std::endl;
    */

    // since we have unweighted events, we should store event.weight = +1 
    std::cout << "         "
	      << event.eventNumber << " " 
      	      << event.weight << " " 
	      << event.Q << std::endl;
  
    for (int i=0;i<2;i++) {
      std::cout << std::setw(3);
      std::cout << event.incoming[i].pid << " "
		<< event.incoming[i].color1 << " "
		<< event.incoming[i].color2 << " "
		<< std::setw(16)
		<< event.incoming[i].pz << std::endl;
    }
    for (int i=0;i<5;i++) {
      std::cout << std::setw(3);
      std::cout << event.outgoing[i].pid << " "
		<< event.outgoing[i].color1 << " "
		<< event.outgoing[i].color2 << " "
		<< std::setw(16)
		<< event.outgoing[i].p[0] << " "
		<< event.outgoing[i].p[1] << " "
		<< event.outgoing[i].p[2] << std::endl;
    }
    if (iEvent==nEvents-(start-1)) exit(0);
    iEvent++;
  }
  
  return 1;
}
